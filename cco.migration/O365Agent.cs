﻿using Microsoft.SharePoint.Client;
using SP=Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.IO;

namespace CCO.Migration
{
    public class O365Agent
    {
        List<String> copyFields;
        int itemCount = 0;

        public SharePointOnlineCredentials getCredentials(String url, String userName, String password)
        {
            var passWord = new SecureString();
            foreach (char c in password.ToCharArray()) passWord.AppendChar(c);
            return new SharePointOnlineCredentials(userName, passWord);
          
        }

        public void copyList(SP.List oList, Web dWebsite, ClientContext dContext, ClientContext oContext, Web oWebsite, string deleteIfExist)
        {
            ListCreationInformation creationInfo = new ListCreationInformation();
            creationInfo.Title = oList.Title;
            creationInfo.TemplateType = oList.BaseTemplate;

            List dList=null;
            try{
                dList = dContext.Web.Lists.GetByTitle(oList.Title);
                dContext.Load(dList);
                dContext.ExecuteQuery();
            }
            catch (Exception e) { dList = null; }

            if(dList==null){
                dList = dWebsite.Lists.Add(creationInfo);
                dContext.Load(dList);
                dContext.ExecuteQuery();
            }else{
                if (deleteIfExist == "yes")
                {
                    dList.DeleteObject();
                    dContext.ExecuteQuery();
                    dList = dWebsite.Lists.Add(creationInfo);
                    dContext.Load(dList);
                    dContext.ExecuteQuery();
                }
            }
            
           


            //Fields in the list
            oContext.Load(oList.Fields);
            oContext.ExecuteQuery();
            dContext.Load(dList.Fields);
            dContext.ExecuteQuery();

            List<Field> dfs = new List<Field>();
            foreach (Field df in dList.Fields){
                dfs.Add(df);
            }
           

            foreach (Field of in oList.Fields) {
                bool existe = false;
                foreach (Field df in dfs)
                {
                    if (df.InternalName == of.InternalName)
                    {
                        df.Title=of.Title; 
                        df.Update();
                        dContext.ExecuteQuery();
                        existe = true;
                        break;
                    }
                 }

                if (!existe) {
                    try
                    {
                        if (of.TypeAsString.Equals("Lookup"))
                        {
                            try
                            {
                              
                                FieldLookup newfl = (FieldLookup)of;


                                List olookupList = oWebsite.Lists.GetById(Guid.Parse(newfl.LookupList));
                                oContext.Load(olookupList);
                                oContext.ExecuteQuery();

                                List dlookupList = dWebsite.Lists.GetByTitle(olookupList.Title);
                                //ListItem i = dlookupList.GetItemById(1);
                                //ListItemCreationInformation ci  = new ListItemCreationInformation();
                                


                                dContext.Load(dlookupList);
                                dContext.ExecuteQuery();
                                //newfl.LookupWebId = dWebsite.Id;
                                //newfl.LookupList = dlookupList.Id.ToString();

                                //newfl.Update();
                                //dList.Update();
                                //dContext.ExecuteQuery();
                                Guid lGuid = Guid.NewGuid();
                                XDocument fieldSchema = XDocument.Parse(newfl.SchemaXml);
 
                                 // Get the root element of the field definition
                                 XElement root = fieldSchema.Root;

                                 XAttribute attrShowField = root.Attribute("ShowField");
                                 if (attrShowField != null)
                                 {
                                     
                                 }

                                 string schemaLookupField = "<Field ID ='{" + lGuid.ToString() + "}' Type='Lookup' Name='" + of.InternalName + "' DisplayName='" + of.InternalName + "' List='" + dlookupList.Id.ToString() + "' ShowField='" + attrShowField.Value + "' />";
                                dList.Fields.AddFieldAsXml(schemaLookupField, true, AddFieldOptions.DefaultValue);
                                dContext.ExecuteQuery();
                            }
                            catch (Exception e) { }                       
                            
                        }
                        else
                        {
                            dList.Fields.AddFieldAsXml(of.SchemaXml, true, AddFieldOptions.AddFieldInternalNameHint);
                            dList.Update();
                            dContext.ExecuteQuery();

                        }
                    }
                    catch (Exception ex) { }
                
                }

                
               
            }
            
            
        
        }

        public void copyListViews(SP.List oList, Web dWebsite, ClientContext dContext, ClientContext oContext, Web oWebsite)
        {

            List dList=null;
            try{

                oContext.Load(oList.Views);
                oContext.ExecuteQuery();

                dList = dContext.Web.Lists.GetByTitle(oList.Title);
                dContext.Load(dList);
                dContext.Load(dList.Views);
                dContext.ExecuteQuery();
            }
            catch (Exception e) { dList = null; }

             foreach(SP.View vw in oList.Views){
                 if (vw.Hidden == false) {
                     ViewCreationInformation vwCreationInfo = new ViewCreationInformation();
                     vwCreationInfo.Paged = vw.Paged; 
                     vwCreationInfo.PersonalView = vw.PersonalView;
                     vwCreationInfo.Query = vw.ViewQuery;
                     vwCreationInfo.RowLimit = vw.RowLimit; 
                     vwCreationInfo.SetAsDefaultView = vw.DefaultView; 
                     vwCreationInfo.Title = vw.Title;
                     int index = 0;

                     oContext.Load(vw.ViewFields);
                     oContext.ExecuteQuery();
                     
                     vwCreationInfo.ViewFields=vw.ViewFields.ToArray();
                     /*foreach(string f in vw.ViewFields){
                         vwCreationInfo.ViewFields
                         vwCreationInfo.ViewFields.SetValue(f, index);
                     }*/

                     vwCreationInfo.ViewTypeKind = SP.ViewType.Html;
                     dList.Views.Add(vwCreationInfo);
                     dContext.ExecuteQuery();
                 }
             } 


        }




        public void copyItems(SP.List oList, Web dWebsite, ClientContext dContext, ClientContext oContext, Web oWebsite, string copyUserFields)
        {
            this.copyFields = new List<String>();
            this.itemCount = 0;
            List<int> sortedList = new List<int>();
            System.Collections.Generic.Dictionary<int, ListItem> _dicListItemColl = new Dictionary<int, ListItem>();

            CamlQuery camlQuery = new CamlQuery();
            camlQuery.ViewXml = "<View><Query><Where><Geq><FieldRef Name='ID'/>" +
                "<Value Type='Number'>0</Value></Geq></Where></Query><RowLimit>5000</RowLimit></View>";

            ListItemCollection lic = oList.GetItems(camlQuery);
           // ListItemCollection lic = oList.GetItems(new CamlQuery());
            oContext.Load(oList.Fields);
            oContext.Load(lic, itms => itms.Include(itm => itm.Id, itm => itm.FieldValuesAsText));
            oContext.ExecuteQuery();

            foreach (ListItem item in lic)
            {
                _dicListItemColl.Add(item.Id, item);
            }
     
            sortedList = _dicListItemColl.Keys.ToList();
            sortedList.Sort();
            List dList = dWebsite.Lists.GetByTitle(oList.Title);
            dContext.Load(dList);
            //if source item Id is not equal to destination item id then create and delete

            //item until source item id and destination item will not be same
            ListItem srcItem = null;
            foreach (var key in sortedList)
            {

                ListItemCreationInformation newItemInfo = null;
                ListItem newItem = null;
                newItemInfo = new ListItemCreationInformation();
                newItem = dList.AddItem(newItemInfo);
                newItem.Update();
                dContext.ExecuteQuery();
                srcItem = _dicListItemColl[key];
                 if (newItem.Id != srcItem.Id && newItem.Id < srcItem.Id){
                    newItem.DeleteObject();
                    int iUpdate = 0;
                    for (int iCount = newItem.Id; iCount <= (srcItem.Id - 2); iCount++){
                        
                        newItemInfo = new ListItemCreationInformation();
                        newItem = dList.AddItem(newItemInfo);
                        newItem.Update();
                        newItem.DeleteObject();
                        iUpdate++;
                        if (iUpdate == 30) {
                            dContext.ExecuteQuery();
                            iUpdate = 0;
                        }
                        
                    }
                    dContext.ExecuteQuery();
                    newItemInfo = new ListItemCreationInformation();
                    newItem = dList.AddItem(newItemInfo);
                    this.copyItem(srcItem, newItem ,oContext, oList, copyUserFields);
                   
                }
                else{
                    this.copyItem(srcItem, newItem, oContext, oList, copyUserFields);
                   
                }
                this.itemCount++;
                try
                {
                    dContext.ExecuteQuery();
                }
                catch (Exception eExectute) { };
            }
        }

        public void copyItem(ListItem oItem, ListItem newItem, ClientContext oContext, List oList, string copyUserFields)
        {
            try
                {
                
                oContext.Load(oItem);
                oContext.ExecuteQuery();



                if (itemCount == 0)
                {
                    foreach (var a in oItem.FieldValuesAsText.FieldValues)
                    {
                        try
                        {
                            Field of = null;
                            of = oList.Fields.GetByInternalNameOrTitle(a.Key);
                            oContext.Load(of);
                            oContext.ExecuteQuery();
                            if (of.ReadOnlyField == false && of.InternalName != "ID" && !of.TypeAsString.Equals("User") && of.InternalName != "Attachments")
                            {
                                newItem[a.Key] = oItem[a.Key];
                                this.copyFields.Add(a.Key);
                            }
                            else if (copyUserFields == "yes" && of.TypeAsString.Equals("User")) {
                                newItem[a.Key] = oItem[a.Key];
                                this.copyFields.Add(a.Key);
                            }
                        }catch (Exception e){}


                        
                    }


                }
                else {
                    foreach (String oField in this.copyFields) {
                        try { 
                        newItem[oField] = oItem[oField];
                        }catch (Exception e) { }
                    }
                }


            /*
            for (int i = 0; i < oItem.Fields.Count; i++)
                if ((!newItem.Fields[i].ReadOnlyField) && (newItem.Fields[i].InternalName != "Attachments"))
                    newItem[newItem.Fields[i].InternalName] = item[newItem.Fields[i].InternalName];
            */
          
            newItem.Update();
            }
            catch (Exception ex)
            {

            }
        }

        public void copyFileItems(SP.List oList, Web dWebsite, ClientContext dContext, ClientContext oContext, Web oWebsite, string copyUserFields, string dummyFileUrl)
        {
            this.copyFields = new List<String>();
            this.itemCount = 0;
            List<int> sortedList = new List<int>();
            System.Collections.Generic.Dictionary<int, ListItem> _dicListItemColl = new Dictionary<int, ListItem>();
            ListItemCollection lic = oList.GetItems(new CamlQuery());
            oContext.Load(oList.Fields);
            oContext.Load(lic, itms => itms.Include(itm => itm.Id, itm => itm.FieldValuesAsText));
            oContext.ExecuteQuery();

            foreach (ListItem item in lic)
            {
                _dicListItemColl.Add(item.Id, item);
            }

            sortedList = _dicListItemColl.Keys.ToList();
            sortedList.Sort();
            List dList = dWebsite.Lists.GetByTitle(oList.Title);
            dContext.Load(dList);
            dContext.ExecuteQuery();
            ListItem srcItem = null;
            int currentId = 1;
            foreach (var key in sortedList)
            {

                FileCreationInformation newFile = new FileCreationInformation();
                SP.File uploadFile = null;
                srcItem = _dicListItemColl[key];
                if (currentId == srcItem.Id)
                {
                    oContext.Load(srcItem.File);
                    oContext.Load(srcItem);
                    SP.ClientResult<Stream> crStream = srcItem.File.OpenBinaryStream();
                    oContext.ExecuteQuery();
                    newFile = new FileCreationInformation();
                    newFile.ContentStream = crStream.Value;
                    newFile.Url = srcItem.File.Name;
                    uploadFile = dList.RootFolder.Files.Add(newFile);
                }
                else {
                    string filePath = dummyFileUrl;
                    newFile.Content = System.IO.File.ReadAllBytes(filePath);
                    newFile.Url = System.IO.Path.GetFileName(filePath);
                    uploadFile = dList.RootFolder.Files.Add(newFile);
                }
                
                try
                {
                    dContext.Load(uploadFile);
                    dContext.Load(uploadFile.ListItemAllFields);
                    dContext.ExecuteQuery();
                    currentId++;
                }
                catch (Exception e) {};
               
                

                SP.ListItem newItem = uploadFile.ListItemAllFields;
                
                if (uploadFile.ListItemAllFields.Id != srcItem.Id && uploadFile.ListItemAllFields.Id < srcItem.Id)
                {
                    uploadFile.ListItemAllFields.DeleteObject();
                    int iUpdate = 0;
                    for (int iCount = uploadFile.ListItemAllFields.Id; iCount <= (srcItem.Id - 2); iCount++)
                    {
                        uploadFile = dList.RootFolder.Files.Add(newFile);
                        uploadFile.ListItemAllFields.DeleteObject();
                        iUpdate++;
                        currentId++;
                        if (iUpdate == 30)
                        {
                            dContext.ExecuteQuery();
                            iUpdate = 0;
                        }

                    }


                      
                    oContext.Load(srcItem.File);
                    oContext.Load(srcItem);
                    SP.ClientResult<Stream> crStream = srcItem.File.OpenBinaryStream();
                    oContext.ExecuteQuery();

                    newFile = new FileCreationInformation();

                    newFile.ContentStream = crStream.Value;
                    newFile.Url = srcItem.File.Name;

                    uploadFile = dList.RootFolder.Files.Add(newFile);
                    //this.copyItem(srcItem, newItem, oContext, oList, copyUserFields);

                }
                else
                {

                    oContext.Load(srcItem.File);
                    oContext.Load(srcItem);
                    SP.ClientResult<Stream> crStream = srcItem.File.OpenBinaryStream();
                    oContext.ExecuteQuery();
                    newFile = new FileCreationInformation();
                    newFile.ContentStream = crStream.Value;
                    newFile.Url = srcItem.File.Name;
                    uploadFile = dList.RootFolder.Files.Add(newFile);
                 //   this.copyItem(srcItem, newItem, oContext, oList, copyUserFields);

                }
                this.itemCount++;
                try
                {
                    dContext.ExecuteQuery();
                    currentId++;
                }
                catch (Exception eExectute) { };
                 
            }
        }

        public void copyFileItem(ListItem oItem, ListItem newItem, ClientContext oContext, List oList, string copyUserFields)
        {
            try
            {

                oContext.Load(oItem);
                oContext.ExecuteQuery();



                if (itemCount == 0)
                {
                    foreach (var a in oItem.FieldValuesAsText.FieldValues)
                    {
                        try
                        {
                            Field of = null;
                            of = oList.Fields.GetByInternalNameOrTitle(a.Key);
                            oContext.Load(of);
                            oContext.ExecuteQuery();
                            if (of.ReadOnlyField == false && of.InternalName != "ID" && !of.TypeAsString.Equals("User") && of.InternalName != "Attachments")
                            {
                                newItem[a.Key] = oItem[a.Key];
                                this.copyFields.Add(a.Key);
                            }
                            else if (copyUserFields == "yes" && of.TypeAsString.Equals("User"))
                            {
                                newItem[a.Key] = oItem[a.Key];
                                this.copyFields.Add(a.Key);
                            }
                        }
                        catch (Exception e) { }



                    }


                }
                else
                {
                    foreach (String oField in this.copyFields)
                    {
                        try
                        {
                            newItem[oField] = oItem[oField];
                        }
                        catch (Exception e) { }
                    }
                }


                /*
                for (int i = 0; i < oItem.Fields.Count; i++)
                    if ((!newItem.Fields[i].ReadOnlyField) && (newItem.Fields[i].InternalName != "Attachments"))
                        newItem[newItem.Fields[i].InternalName] = item[newItem.Fields[i].InternalName];
                */

                newItem.Update();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
