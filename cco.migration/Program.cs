﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Client;
using SP = Microsoft.SharePoint.Client; 
using System.Security;
using System.Xml.Linq;

namespace CCO.Migration
{
    class Program
    {
        static void Main(string[] args)
        {
            O365Agent oAgent = new O365Agent();

            oAgentSettings s = new oAgentSettings("Cfg.xml");
            XElement source=s.getSource();
            XElement target = s.getTarget();

           string url = source.Element("url").Value;
            ClientContext oClientContext = new ClientContext(url);
            oClientContext.Credentials = oAgent.getCredentials(url, source.Element("user").Value, source.Element("password").Value);
            Web oWebsite = oClientContext.Web;
            oClientContext.Load(oWebsite);
            oClientContext.ExecuteQuery();


            string dUrl = target.Element("url").Value;
            ClientContext dClientContext = new ClientContext(dUrl);
            dClientContext.Credentials = oAgent.getCredentials(dUrl, target.Element("user").Value, target.Element("password").Value);

            Web dWebsite = dClientContext.Web;
            dClientContext.Load(dWebsite);
            dClientContext.ExecuteQuery();



            XElement listsElement = s.getTarget();

            IEnumerable<XElement> lists = s.getLists().Elements("list");
            string dummyFileUrl = null;
            try
            {
                dummyFileUrl = s.config.Root.Element("dummyFileUrl").Value;
            }
            catch (Exception e) { };
            foreach(XElement l in lists){

                string lista = l.Element("name").Value;
                string delete = l.Element("deleteIfExist").Value;
                string copyUserFields = l.Element("copyUserFields").Value;
               


                List oList = oClientContext.Web.Lists.GetByTitle(lista);
                oClientContext.Load(oList);
                oClientContext.ExecuteQuery();
                //copia estructura
               oAgent.copyList(oList, dWebsite, dClientContext, oClientContext, oWebsite, delete);

                dClientContext = new ClientContext(dUrl);
                dClientContext.Credentials = oAgent.getCredentials(dUrl, target.Element("user").Value, target.Element("password").Value);
                dWebsite = dClientContext.Web;
                dClientContext.Load(dWebsite);
                dClientContext.ExecuteQuery();

               // oAgent.copyListViews(oList, dWebsite, dClientContext, oClientContext, oWebsite);


                dClientContext = new ClientContext(dUrl);
                dClientContext.Credentials = oAgent.getCredentials(dUrl, target.Element("user").Value, target.Element("password").Value);
                dWebsite = dClientContext.Web;
                dClientContext.Load(dWebsite);
                dClientContext.ExecuteQuery();
                if (oList.BaseTemplate == 115)
                {
                    oAgent.copyFileItems(oList, dWebsite, dClientContext, oClientContext, oWebsite, copyUserFields, dummyFileUrl);
                }
                else {
                   oAgent.copyItems(oList, dWebsite, dClientContext, oClientContext, oWebsite, copyUserFields);
                }
                

            }

            
           // Console.Read();
        }


       
    }
}
