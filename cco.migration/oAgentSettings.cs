﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CCO.Migration
{
    public class oAgentSettings
    {

        public XDocument config;
        public oAgentSettings(String uri )
        {
            config = XDocument.Load(uri);
            
        }
        public XElement getSource() {
            XElement source = config.Root.Element("source");
            return source;
        }

        public XElement getTarget()
        {
            XElement source = config.Root.Element("target");
            return source;
        }


        public XElement getLists()
        {
            XElement lists = config.Root.Element("lists");
            return lists;
        }
    }
}
